﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using WillTartakDevTest.Common;
using WillTartakDevTest.CosmosDb;

namespace DbTester
{
	class StartHere
	{
		static void Main(string[] args)
		{
			// First setup the Configuration & Dependency Injection environment
			var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
			var builder = new ConfigurationBuilder()
				 .SetBasePath(Directory.GetCurrentDirectory())
				 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

			var config = builder.Build();

			var appSettings = new AppSettings();
			config.GetSection("AppSettings").Bind(appSettings);
			var dbMgr = new DbManager();
			dbMgr.ExecuteAsync(appSettings).GetAwaiter().GetResult();

		}

	}
}