﻿using System;

namespace WillTartakDevTest.Common
{
	public class AppSettings
	{
		public string ApiUrlBase { get; set; }
		public string ApplicationName { get; set; }
		public string CDbEndPoint { get; set; }
		public string CDbKey { get; set; }
		public string CDbName { get; set; }
		public string CDbSetName { get; set; }
		public string WebUrlBase { get; set; }


	}

}
