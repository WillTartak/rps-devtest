﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WillTartakDevTest.CosmosDb
{

	public class Policy
	{
		public Policy()
		{

		}


		public Policy(string policyId, string policyNumber, DateTime effDate, DateTime expDate, List<Insured> insureds, List<Risk> risks)
		{
			this.PolicyId = policyId;
			this.PolicyNumber = policyNumber;
			this.EffectiveDate = effDate;
			this.ExpirationDate = expDate;
			this.Insureds = insureds;
			this.Risks = risks;

		}

		//[Key]
		[JsonProperty(PropertyName = "id")]
		public string PolicyId { get; set; }
		public string PolicyNumber { get; set; }
		public DateTime EffectiveDate { get; set; }
		public DateTime ExpirationDate { get; set; }
		public List<Insured> Insureds { get; set; }
		public List<Risk> Risks { get; set; }

	}


	public class Insured
	{
		//[Key]
		[JsonProperty(PropertyName = "id")]
		public string InsuredId { get; set; }
		public string InsuredName { get; set; }
		public string MailingAddress { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }
		public bool IsPrimary { get; set; }

	}


	public class Risk
	{
		//[Key]
		[JsonProperty(PropertyName = "id")]
		public string RiskId { get; set; }
		public string RiskConstruction { get; set; }
		public string YearBuilt { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string ZipCode { get; set; }

	}

}
