﻿using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WillTartakDevTest.Common;

namespace WillTartakDevTest.CosmosDb
{
	// borrowed from https://github.com/ealsur/auth0documentdb/blob/master/Services/DocumentDbProvider.cs then modified to match other system parts.
	/// <summary>
	/// DocumentDB provider for wrapping common accessors
	/// </summary>
	public class DocumentDbProvider : IDocumentDbProvider
	{
		private readonly AppSettings _settings;
		private readonly Uri _collectionUri;
		private DocumentClient _dbClient;
		public DocumentDbProvider(IOptions<AppSettings> settings)
		{
			_settings = settings.Value;
			_collectionUri = GetCollectionLink();
			//See https://azure.microsoft.com/documentation/articles/documentdb-performance-tips/ for performance tips
			_dbClient = new DocumentClient(new Uri(_settings.CDbEndPoint), _settings.CDbKey, new ConnectionPolicy()
			{
				MaxConnectionLimit = 100,
				ConnectionMode = ConnectionMode.Direct,
				ConnectionProtocol = Protocol.Tcp
			});

			_dbClient.OpenAsync().Wait();

		}

		#region Private
		/// <summary>
		/// Obtains the link of a collection
		/// </summary>
		/// <param name="databaseName"></param>
		/// <param name="collectionName"></param>
		/// <returns></returns>
		private Uri GetCollectionLink()
		{
			return UriFactory.CreateDocumentCollectionUri(_settings.CDbName, _settings.CDbSetName);
		}

		/// <summary>
		/// Obtains the link for a document
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		private Uri GetDocumentLink(string id)
		{
			return UriFactory.CreateDocumentUri(_settings.CDbName, _settings.CDbSetName, id);
		}
		#endregion



		#region Public
		/// <summary>
		/// Creates a Query with FeedOptions
		/// </summary>
		/// <typeparam name="T">Type of Class to serialize</typeparam>
		/// <param name="feedOptions"></param>
		/// <returns></returns>
		/// <comment>Ideally this would be async</comment>
		public IEnumerable<T> CreateQuery<T>(FeedOptions feedOptions)
		{
			return _dbClient.CreateDocumentQuery<T>(_collectionUri, feedOptions).ToList();
		}

		/// <summary>
		/// Creates a Query with FeedOptions and a SQL expression
		/// </summary>
		/// <typeparam name="T">Type of Class to serialize</typeparam>
		/// <param name="sqlExpression">SQL query</param>
		/// <param name="feedOptions"></param>
		/// <returns></returns>
		public IQueryable<T> CreateQuery<T>(string sqlExpression, FeedOptions feedOptions)
		{
			return _dbClient.CreateDocumentQuery<T>(_collectionUri, sqlExpression, feedOptions).ToList().AsQueryable();
		}

		/// <summary>
		/// Adds an item to a collection
		/// </summary>
		/// <typeparam name="T">Type of Class to serialize</typeparam>
		/// <param name="document">Document to add</param>
		/// <returns></returns>
		public async Task<string> AddItem<T>(T document)
		{
			var result = await _dbClient.CreateDocumentAsync(_collectionUri, document);
			return result.Resource.Id;
		}

		/// <summary>
		/// Updates a document on a collection
		/// </summary>
		/// <typeparam name="T">Type of Class to serialize</typeparam>
		/// <param name="document">Document to add</param>
		/// <param name="id">Id of the document to update</param>
		/// <returns></returns>
		public async Task<string> UpdateItem<T>(T document, string id)
		{
			var result = await _dbClient.ReplaceDocumentAsync(GetDocumentLink(id), document);
			return result.Resource.Id;
		}

		/// <summary>
		/// Deletes a document from a collection
		/// </summary>
		/// <param name="id">Id of the document to delete</param>
		/// <returns></returns>
		public async Task DeleteItem(string id)
		{
			await _dbClient.DeleteDocumentAsync(GetDocumentLink(id));
		}

		#endregion
	}

}
