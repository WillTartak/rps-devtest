﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WillTartakDevTest.Common;

namespace WillTartakDevTest.CosmosDb
{
	public class DbManager
	{
		string _dbUrl;
		string _authKey;
		bool _addSmapleData = false;

		DocumentClient dbClient = null;
		DocumentCollection dbSet = null;
		Document dbDoc = null;

		public async Task ExecuteAsync(AppSettings settings, bool addSampleData = true)
		{
			_dbUrl = settings.CDbEndPoint;
			_authKey = settings.CDbKey;
			_addSmapleData = addSampleData;

			try
			{
				var database = createDatabase(settings.CDbName).Result;
				await createDocumentCollection(database, settings.CDbSetName);

			}
			catch (Exception ex)
			{
				Console.WriteLine("Error Occured " + ex.Message);

			}

		}


		//Method to Create a Database in DocumentDb
		async Task<Database> createDatabase(string dbName)
		{
			dbClient = new DocumentClient(new Uri(_dbUrl), _authKey);
			Database dbDatabase = dbClient.CreateDatabaseQuery().Where(d => d.Id == dbName).AsEnumerable().FirstOrDefault();
			if (dbDatabase == null)
			{
				dbDatabase = await dbClient.CreateDatabaseAsync(new Database()
				{
					Id = dbName

				});

				Console.WriteLine("The DocumentDB of name " + dbName + " created successfully.");

			}
			else
			{
				Console.WriteLine("This Database is already available");

			}

			return dbDatabase;

		}


		async Task createDocumentCollection(Database db, string setName)
		{
			dbSet = dbClient.CreateDocumentCollectionQuery("dbs/" + db.Id).Where(c => c.Id == setName).AsEnumerable().FirstOrDefault();
			
			if (dbSet == null)
			{
				dbSet = await dbClient.CreateDocumentCollectionAsync("dbs/" + db.Id,
					 new DocumentCollection
					 {
						 Id = setName
					 });

				Console.WriteLine("Created dbs" + db.Id + "Collection " + setName);


			}
			
			await createDocumentData(db, dbSet.Id, "1");

		}


		async Task createDocumentData(Database db, string setName, string docName)
		{
			if (_addSmapleData)
			{
				dbDoc = dbClient.CreateDocumentQuery(dbSet.SelfLink)
										  .Where(d => d.Id == docName).AsEnumerable().FirstOrDefault();
				if (dbDoc == null)
				{
					dynamic primer = new Policy()
					{
						PolicyId = "1",
						PolicyNumber = "100000000",
						EffectiveDate = DateTime.Now,
						ExpirationDate = DateTime.Now.AddYears(1),
						Insureds = new List<Insured>()
						{
							new Insured()
							{
								InsuredId = "1",
								InsuredName = "Primary Insured",
								MailingAddress = "111 Main Street",
								City = "Sometown",
								State = "IL",
								ZipCode = "11111",
								IsPrimary = true
							},
							new Insured()
							{
								InsuredId = "2",
								InsuredName = "Secondary Insured",
								MailingAddress = "222 Main Street",
								City = "Sometown",
								State = "IL",
								ZipCode = "11111",
								IsPrimary = false
							}
						},
						Risks = new List<Risk>()
						{
							new Risk()
							{
								RiskId = "1",
								RiskConstruction = "Modular Home",
								YearBuilt = "2010",
								Address = "1234 Maple St",
								City = "Somewhere",
								State = "IL",
								ZipCode = "11111"

							}
						}

					};

					var result = await dbClient.CreateDocumentAsync(dbSet.SelfLink, primer);

					Console.WriteLine("Created dbs/" + db.Id + "/colls/" + setName + "/docs/" + docName);

				}

			}

		}

	}

}
