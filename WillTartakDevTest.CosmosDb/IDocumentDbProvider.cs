﻿using Microsoft.Azure.Documents.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WillTartakDevTest.CosmosDb
{
	public interface IDocumentDbProvider
	{
		IEnumerable<T> CreateQuery<T>(FeedOptions feedOptions);
		IQueryable<T> CreateQuery<T>(string sqlExpression, FeedOptions feedOptions);

		Task<string> AddItem<T>(T document);
		Task<string> UpdateItem<T>(T document, string id);
		Task DeleteItem(string id);

	}

}
