import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'primeng/primeng';

import { AppComponent } from './components/app/app.component'
import { HomeComponent } from './components/home/home.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { PolicyAddComponent } from './components/policy/policyadd.component';
import { PolicyComponent } from './components/policy/policy.component';

export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        PolicyAddComponent,
        PolicyComponent
    ],
    imports: [
       RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'policy', component: PolicyComponent },
            { path: 'policyadd', component: PolicyAddComponent },
            { path: '**', redirectTo: 'home' }
       ]),
       BrowserAnimationsModule,
       CalendarModule,
       FormsModule,
       ReactiveFormsModule
    ]
};
