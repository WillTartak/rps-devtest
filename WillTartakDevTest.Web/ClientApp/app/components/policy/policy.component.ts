﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { Policy } from './policy.model';

@Component({
   selector: 'policy',
   template: require('./policy.component.html')
})
export class PolicyComponent {
   public model: Array<Policy>;

   constructor(private http: Http, private router: Router) {  
   }


   addPolicy() {
      this.router.navigate(['/policyadd']);
   }


   getPolicies(policyNo: string, insuredName: string) {
      var qs = "";
      if (policyNo || insuredName) {
         if (!policyNo)
            policyNo = "";

         qs = "/GetByPolicyNoName?policyNumber=" + policyNo;
         if (insuredName)
            qs = qs + "&name=" + insuredName;
      }

      this.model = null;
      this.http.get('http://localhost:64070/api/policy' + qs).subscribe(result => {
         var json = result.json();
         if (json.length > 0)
            this.model = result.json() as Array<Policy>;
      });

   }

}
