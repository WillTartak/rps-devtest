﻿export class Risk
{
   constructor(
      public id: string,
      public riskConstruction: string,
	   public yearBuilt?: string,
      public address?: string,
      public city?: string,
      public state?: string,
      public zipCode?: string

      )
   {}

}