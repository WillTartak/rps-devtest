﻿import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CalendarModule } from 'primeng/primeng';
import { Policy } from './policy.model';
import { Insured } from './insured.model';
import { Risk } from './risk.model';

@Component({
   selector: 'policy-form',
   templateUrl: './policyadd.component.html',
   styleUrls: ['./policyadd.component.css']
})
export class PolicyAddComponent implements OnInit {
   form: FormGroup;
   message: string;
   model: Policy;
   riskSelections = ['Site Built Home', 'Modular Home', 'Single Manufactured Home', 'Double Wide Manufactured Home'];
   showMessage: boolean = false;
   submitted: boolean = false;
   // TODO: Should create a state array and dropdown

   constructor(private http: Http, private router: Router, private fb: FormBuilder)
   {
   }


   ngOnInit() {

      this.form = this.fb.group({
         "id": [""],
         "policyNumber": ["", [Validators.required, Validators.minLength(4)]],
         "effectiveDate": ["", Validators.required],
         "expirationDate": ["", Validators.required],
         "insureds": this.fb.array([
            this.initInsured()
         ]),
         "risks": this.fb.array([
            this.initRisk()
         ])


      });
   }


   initInsured() {
      // initialize named insured:
      return this.fb.group({
         "id": [""],
         "insuredName": ['', Validators.required],
         "mailingAddress": [''],
         "city": [''],
         "state": [''],
         "zipCode": [''],
         "isPrimary": ['']
         //TODO: isPrimary needs a custom validator to only allow one true per Policy.
      });
   }


   addInsured() {
      // add insured to the list
      const control = <FormArray>this.form.controls['insureds'];
      control.push(this.initInsured());
   }


   removeInsured(i: number) {
      // remove insured from the list
      const control = <FormArray>this.form.controls['insureds'];
      control.removeAt(i);
   }


   initRisk() {
      return this.fb.group({
         "id": [""],
         "riskConstruction": ['', Validators.required],
         "yearBuilt": [''],
         "address": [''],
         "city": [''],
         "state": [''],
         "zipCode": ['']

      })
      
   }


   addRisk() {
      // add risk to the list
      const control = <FormArray>this.form.controls['risks'];
      control.push(this.initRisk());
   }


   removeRisk(i: number) {
      // remove risk from the list
      const control = <FormArray>this.form.controls['risks'];
      control.removeAt(i);
   }


   save(model) {
      this.submitted = true;
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      this.cleanModel(model.value);
      //TODO: this url should come from configuration.
      this.http.post("http://localhost:64070/api/policy", model.value, options).subscribe(
         (data: any) => {
            this.showMessage = true;
            this.message = "The policy was saved successfully.";
            console.log(data);
            //TODO: Need to find a way to pass a message to the list/search page.
            this.router.navigate(['/policy']);
         },
         function (error) {
            console.log(error);
            this.showMessage = true;
            this.message = "There was a problem saving the Policy."
         });
      
   }


   cleanModel(model: Policy)
   {
      if (!model.id)
         model.id = this.generateUUID();

      model.insureds.forEach((ins) =>
      {
         if (!ins.id)
            ins.id = this.generateUUID();

         if (!ins.isPrimary)
            ins.isPrimary = false;

      });

      model.risks.forEach((risk) =>
      {
         if (!risk.id)
            risk.id = this.generateUUID();

      })

   }


   generateUUID() { // Public Domain/MIT
      var d = new Date().getTime();
      if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
         d += performance.now(); //use high-precision timer if available
      }
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
         var r = (d + Math.random() * 16) % 16 | 0;
         d = Math.floor(d / 16);
         return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
      });
   }

}