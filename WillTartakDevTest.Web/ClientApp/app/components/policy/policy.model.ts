﻿import { Insured } from './insured.model';
import { Risk } from './risk.model';

export class Policy {

   constructor(
         public id: string,
         public policyNumber: string,
         public effectiveDate: Date,
         public expirationDate: Date,
         public insureds?: Array<Insured>,
         public risks?: Array<Risk>

      )
   {
   }
   
}