﻿export class Insured {

   constructor(
      public id: string,
      public insuredName: string,
      public mailingAddress?: string,
      public city?: string,
      public state?: string,
      public zipCode?: string,
      public isPrimary?: boolean
   )
   {}

}