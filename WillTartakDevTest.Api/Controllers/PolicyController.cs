﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WillTartakDevTest.CosmosDb;
using Microsoft.Azure.Documents.Client;
using Newtonsoft.Json;

namespace WillTartakDevTest.Api.Controllers
{
	[Route("api/[controller]")]
	public class PolicyController : Controller
	{
		private readonly IDocumentDbProvider _dbProvider;

		public PolicyController(IDocumentDbProvider provider)
		{
			_dbProvider = provider;
		}


		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				FeedOptions opts = getDefaultOptions();
				var result = _dbProvider.CreateQuery<Policy>(opts);

				return Ok(result);

			}
			catch (Exception ex)
			{
				return BadRequest($"Error getting Policy data: {ex.Message}");

			}

		}


		[HttpGet, Route("[action]")]
		public IActionResult GetByPolicyNoName([FromQuery]string policyNumber, [FromQuery]string name)
		{
			try
			{
				var sql = string.Format("SELECT p.id, p.PolicyNumber, p.EffectiveDate, p.ExpirationDate, p.Insureds, p.Risks FROM Policy p {0} WHERE ", !string.IsNullOrWhiteSpace(name) ? "JOIN i IN p.Insureds" : "");
				if (!string.IsNullOrWhiteSpace(policyNumber))
					sql += $" p.PolicyNumber='{policyNumber}'";

				if (!string.IsNullOrWhiteSpace(name))
				{
					if (sql.Contains("PolicyNumber="))
						sql += " AND ";

					sql += $"i.InsuredName='{name}'";

				}

				FeedOptions opts = getDefaultOptions();
				var result = _dbProvider.CreateQuery<Policy>(sql, opts);

				return Ok(result);

			}
			catch (Exception ex)
			{
				return BadRequest($"Error getting Policy data: {ex.Message}");

			}

		}


		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			try
			{
				FeedOptions opts = getDefaultOptions(1);
				var result = _dbProvider.CreateQuery<Policy>($"SELECT * from c WHERE c.id = '{id}'", opts);

				return Ok(result);

			}
			catch (Exception ex)
			{
				return BadRequest($"Error getting Policy data: {ex.Message}");

			}

		}


		[HttpPost]
		public async Task<IActionResult> Post([FromBody] Policy model)
		{
			try
			{
				//var model = JsonConvert.DeserializeObject<Policy>(value);
				fillIds(model);
				var result = await _dbProvider.AddItem<Policy>(model);
				if (!string.IsNullOrWhiteSpace(result))
				{
					return Created($"{Request.Scheme}://{Request.Host}{Request.Path.ToUriComponent()}/{result}", model);

				}
				else
				{
					return BadRequest();

				}

			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);

			}

		}


		[HttpPut("{id}")]
		public async Task<IActionResult> Put(string id, [FromBody]Policy value)
		{
			fillIds(value);
			var result = await _dbProvider.UpdateItem<Policy>(value, id);
			if (!string.IsNullOrWhiteSpace(result))
			{
				return Ok(result);

			}
			else
			{
				return BadRequest();

			}

		}



		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(string id)
		{
			await _dbProvider.DeleteItem(id);

			return Ok();

		}


		private void fillIds(Policy model)
		{
			if (model != null)
			{
				if (string.IsNullOrWhiteSpace(model.PolicyId))
					model.PolicyId = Guid.NewGuid().ToString();

				foreach (var ins in model.Insureds)
				{
					if (string.IsNullOrWhiteSpace(ins.InsuredId))
						ins.InsuredId = Guid.NewGuid().ToString();

				}

				foreach (var risk in model.Risks)
				{
					if (string.IsNullOrWhiteSpace(risk.RiskId))
						risk.RiskId = Guid.NewGuid().ToString();

				}

			}

		}


		private static FeedOptions getDefaultOptions(int maxCount = 1000)
		{
			var opts = new FeedOptions();
			opts.MaxBufferedItemCount = maxCount;
			return opts;
		}

	}

}